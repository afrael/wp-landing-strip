<?php
/*
Plugin Name: Landing Strip - Landing Pages made easy -
Plugin URI: http://afraelortiz.com/projects/landingstrip
Description: Plugin that serves as a theme installer. 
Author: Afrael Ortiz
Version: 0.1
Author URI: http://afraelortiz.com/
*/

/*
	Define the available page templates
*/

global $LSThemeTemplates;
$LSThemeTemplates = array();

// Do not edit anything above this line
// Customize here with your theme folders, substitute with the names of the folder you wish to install, delete any unused lines.
$LSThemeTemplates[] = 'template1';
$LSThemeTemplates[] = 'template2';
$LSThemeTemplates[] = 'template3';
// Do not edit anything below this line

global $LSPluginHomeDirectory;
$LSPluginHomeDirectory = '';

// register basic activation and deactivation hooks
register_activation_hook(__FILE__, 'LSActivate');
register_deactivation_hook(__FILE__, 'LSDeactivate');

// register plugin initialization hook
add_action('init', 'LSActivate');

function LSActivate()
{
	LSInstallUninstallPlugin('install');
}

function LSDeactivate()
{
	LSInstallUninstallPlugin('uninstall');
}

/*
	Core installation/uninstallation function
	$action allowed valued are:
		- install
		- uninstall
*/
function LSInstallUninstallPlugin($action)
{
	// set plugin home folder
	$LSPluginHomeDirectory = dirname(__FILE__) . '/wp-landing-strip';

	// get the current theme directory
	$currentThemeDirectory = get_stylesheet_directory();
	$desiredPermissions = 0777;
	$actualPermissions = fileperms($currentThemeDirectory);
	if($actualPermissions < $desiredPermissions)
	{
		if(!chmod($currentThemeDirectory, $desiredPermissions))
		{
			add_action('admin_notices', 'LSSetPermissionsFailed');	
		}
		else
		{
			switch ($action) {
				case 'install':
					LSInstallPageTemplates($currentThemeDirectory);
					break;

				case 'uninstall':
					LSUninstallPageTemplates($currentThemeDirectory);
					break;
			}
			// reset the original permissions to patch any holes caused by rogue permissions
			chmod($currentThemeDirectory, $actualPermissions);
		}
	}	
}

function LSInstallPageTemplates($currentThemeDirectory)
{
	$destinationPath = '';
	foreach($LSThemeTemplates as $templateName)
	{
		$destinationPath = $currentThemeDirectory . '/' . $templateName;
		// check to see if the destination path exists
		if(!file_exists($destinationPath))
		{
			if(!mkdir($destinationPath))
				add_action('admin_notices', 'LSDestinationDirectoryCreationFailed');	
			else
				LSCopyTemplateToDesinationFolder($templateName, $destinationPath);
		}
		else
			LSCopyTemplateToDesinationFolder($templateName, $destinationPath);
	}
}

function LSCopyTemplateToDesinationFolder($templateName, $destinationPath)
{
	$originDirectory = $LSPluginHomeDirectory . '/' . $templateName;
	LSCopyFilesInDirectory($originDirectory, $destinationPath);
}

function LSCopyFilesInDirectory($originDirectory, $destinationDirectory)
{
	foreach(new DirectoryIterator($originDirectory) as $fileHandle)
	{
		if(!$fileHandler->isDot() && !$fileHandle->isDir() && $fileHandle->isFile())
		{
			$originFile = $originDirectory . '/' . $fileHandle->getFileName();
			$destinationFile = $destinationDirectory . '/' . $fileHandle->getFileName();
			if(file_exists($destinationFile))
			{
				// delete and copy
				if(!unlink($destinationFile))
					add_action('admin_notices', 'LSTemplateFileCopyFailed');	
				else
				{
					if(!copy($originFile, $destinationFile))
						add_action('admin_notices', 'LSTemplateFileCopyFailed');	
				}
			}
			else
			{
				if(!copy($originFile, $destinationFile))
					add_action('admin_notices', 'LSTemplateFileCopyFailed');	
			}
		}
		elseif(!$fileHandler->isDot() && $fileHandle->isDir() && !$fileHandle->isFile())
		{
			$newOriginDirectory = $originDirectory . '/' . $fileHandle->getFileName();
			$newDestinationDirectory = $destinationDirectory . '/' . $fileHandle->getFileName();
			if(!file_exists($newDestinationDirectory))
			{
				if(!mkdir($newDestinationDirectory))
					add_action('admin_notices', 'LSDestinationDirectoryCreationFailed');
			}
			LSCopyFilesInDirectory($newOriginDirectory, $newDestinationDirectory);
		}
	}
}

function LSUninstallPageTemplates($currentThemeDirectory)
{
	$targetPath = '';
	foreach($LSThemeTemplates as $templateName)
	{
		$targetPath = $currentThemeDirectory . '/' . $templateName;
		// check to see if the destination path exists
		if(file_exists($targetPath))
		{
			LSRemoveFolder($targetPath);
		}
	}
}

function LSRemoveFolder($targetPath)
{
	foreach(new DirectoryIterator($targetPath) as $fileHandle)
	{
		if(!$fileHandler->isDot() && !$fileHandle->isDir() && $fileHandle->isFile())
		{
			// delete and copy
			if(!unlink($destinationFile))
				add_action('admin_notices', 'LSTemplateUninstallFailed');	
		}
		elseif(!$fileHandler->isDot() && $fileHandle->isDir() && !$fileHandle->isFile())
		{
			$newTargetPath = $targetPath . '/' . $fileHandle->getFileName();
			LSRemoveFolder($newTargetPath);
			if(!rmdir($fileHandle->getFileName()))
				add_action('admin_notices', 'LSTemplateUninstallFailed');	
		}
	}
}


/*
	Error wrapper function
	Template uninstallation Failed
*/
function LSTemplateUninstallFailed()
{
	$error = 'The plugin was unable uninstall the template.';
	LSDisplayError($error);
}

/*
	Error wrapper function
	Template file copy Failed
*/
function LSTemplateFileCopyFailed()
{
	$error = 'The plugin was unable to copy the template files to the destination folder.';
	LSDisplayError($error);
}

/*
	Error wrapper function
	Directory Creation Failed
*/
function LSDestinationDirectoryCreationFailed()
{
	$error = 'The plugin was unable to create the destination folder for the landing page template.';
	LSDisplayError($error);
}

/*
	Error wrapper function
	Changing the directory permissions failed
*/
function LSSetPermissionsFailed()
{
	$error = 'The plugin was unable to set the appropriate permissions to successfully install itself.';
	LSDisplayError($error);
}


/*
	Generic error function
*/
function LSDisplayError($errorText)
{
	echo '<div class=\'error>\'' . "$errorText" . '</div>';
}

?>